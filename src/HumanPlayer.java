import java.util.List;

public class HumanPlayer implements Player {

    private List<Card> hand;
    Deck deck = new Deck();



    @Override
    public List<Card> getHand() {
        return hand;
    }

    @Override
    public Card drawCard() {
        Card card = deck.drawCard();
        return card;
    }

    @Override
    public Card playCard() {
        return null;
    }

    @Override
    public void setHand(List<Card> hand) {
        this.hand = hand;
    }

    @Override
    public Card CardinRotation() {
        return null;
    }

    @Override
    public void callUno() {

    }
    // Checlkist:
    // player's hand of cards array
    // ability to draw a card
    // ability to place a card onto drawpile
    // see their current hand
    // see the current card in playpile rotation
    // player needs to call uno when 1 card is left (create function called draw 4 in case uno is not called on time
}



