import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Game {

    private Player currentPlayer;

    private int noOfPlayers;

    private Deck deck;

    private List<Card> remainingDeck;

    private Card initial;


    private List<Card> playerHand;

    private Player[] players;

    private List<Card> playPile;

    private Card.Color validColor;
    private Card.Value validValue;

    boolean direction;

    public Game(int pNum) {
        deck = new Deck();
        deck.shuffle();
        playPile = new ArrayList<Card>();

        noOfPlayers = pNum;
        direction = false; // false: anti-clockwise
                           // true: clockwise

        playerHand = new ArrayList<Card>();
        players = new HumanPlayer[noOfPlayers];
        for (int i = 0; i < noOfPlayers; i++) {
            players[i] = new HumanPlayer();
            players[i].setHand(Arrays.asList(deck.drawMultiple(10)));
        }
        currentPlayer = players[0];
    }

//    public void switchTurn(){
//        if(currentPlayer == 0){
//            currentPlayer = 1;
//        } else {
//            currentPlayer = 0;
//        }
//    }

    public void start() {
        if(deck == null){
            System.out.println("You fucked up");
        }
        initial = deck.drawCard();      //this should drawn from the remaining deck
        System.out.println("initial card: " + initial);
        validColor = initial.getColor();
        validValue = initial.getValue();

        // if game starts with a WILD or a WILD+4, then it is placed back into the deck and then shuffled, and the new topcard is placed
        if (initial.getValue() == Card.Value.Wild || initial.getValue() == Card.Value.Wild_Four) {
            deck.add(initial);
            deck.shuffle();
            initial = deck.drawCard();
        }

        // initial card is a draw two
        if (initial.getValue() == Card.Value.DrawTwo) {
            currentPlayer.getHand().addAll(Arrays.asList(deck.drawMultiple(2)));
        }

        // initial card is a skip
        if (initial.getValue() == Card.Value.Skip) {
            if (direction) {
                currentPlayer = players[getClockNext()];
            } else {
                currentPlayer = players[getAntiClockNext()];
            }
        }

        // initial card is a reverse, direction is changed
        if (initial.getValue() == Card.Value.Reverse) {
            direction = true;
        }
        playPile.add(initial);
    }

    public boolean emptyHand(Player player) {
        if (player.getHand().size() == 0) {
            return true;
        }
        return false;
    }


    public Card getTopCard() {
        return new Card(validColor, validValue);
    }

    public int getClockNext() {
        for (int i = 0; i < players.length; i++) {
            if (currentPlayer == players[i]) {
                return i + 1;
            }
        }
        return 0;
    }

    public int getAntiClockNext() {
        for (int i = 0; i < players.length; i++) {
            if (currentPlayer == players[0]) {
                return players.length - 1;
            }
            if (currentPlayer == players[i]) {
                return i - 1;
            }
        }
        return 0;
    }


    public boolean gameOver() {
        for (int i = 0; i < players.length; i++) {
            if (emptyHand(players[i])) {
                return true;
            }
        }
        return false;
    }



//    public Card getPlayerCard(HumanPlayer player, int choice) {
//        List<Card> hand = player.getHand();
//        return playerHand.get(choice);
//    }

    public boolean validCardPlay(Card card) {
        return card.getColor() == validColor || card.getValue() == validValue;
    }

    public void checkPlayerTurn(String pNum) {
        for (int i = 0; i < players.length; i++) {
            if (currentPlayer != players[i]) {
                System.out.println("It is not " + pNum + " 's turn");
            }
        }
    }

    public void replaceDeckWithPlayPile(String pNum) {
        checkPlayerTurn(pNum);
        if (deck.isEmptyDeck()) {
            deck.replaceDeckWith(playPile);
            deck.shuffle();
        }
    }

    public void setCardColor(Card.Color color) {
        validColor = color;
    }

    public void submitPlayerCard(Player pid, Card card, Card.Color declaredColor) throws
            InvalidColorSubmissionException, InvalidValueSubmissionException, InvalidPlayerTurnException {

    }



    public void printStatus(){
        for (int i = 0; i < players.length; i++) {
            System.out.println(players[i].getHand());
        }
    }

    class InvalidPlayerTurnException extends  Exception {
        String playerIds;

        public InvalidPlayerTurnException(String pid) {
            playerIds = pid;
        }

        public String getPid() {
            return playerIds;
        }
    }

    class InvalidColorSubmissionException extends Exception {
        private Card.Color expected;
        private Card.Color actual;

        public InvalidColorSubmissionException(Card.Color actual, Card.Color expected) {
            this.actual = actual;
            this.expected = expected;
        }
    }

    class InvalidValueSubmissionException extends Exception {
        private Card.Value expected;
        private Card.Value actual;

        public InvalidValueSubmissionException(Card.Value actual, Card.Value expected) {
            this.actual = actual;
            this.expected = expected;
        }
    }

    public static void main(String[] args) {
        Game game = new Game(2);
        game.start();
        game.printStatus();
    }
}
