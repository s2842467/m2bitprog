import java.util.List;
interface Player {

        public List getHand();

        public Card drawCard();

        public Card playCard();

        public void setHand(List<Card> hand);

        public Card CardinRotation();

        public void callUno();

}
